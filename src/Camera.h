#pragma once

#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include "MatrixStack.h"

class Camera {

public:
    static const float GOLD_THETA;
    static const float GOLD_ALPHA;
    static const float SILVER_THETA;
    static const float SILVER_ALPHA;
    static const float DEFAULT_RADIUS;

    Camera() = default;
    ~Camera() = default;

    void init();
    void rotateCamera(const float deltaTheta, const float deltaAlpha, const float deltaZoom);
    void prepareForReposition(const float endTheta, const float endAlpha);
    bool updateReposition();
    void applyCamera(std::shared_ptr<MatrixStack> V);

private:
    static const int FRAMES;
    static const glm::vec3 LOOK_AT;
    static const glm::vec3 UP;

    static const glm::vec3 initUp();
    static const glm::vec3 initLookAt();

    void updateEyeVec();

    float theta;
    float alpha;
    float radius;
    glm::vec3 eye;

    // variables for camera auto-repositioning
    bool isRepositioning;
    float repoOldTheta;
    float repoOldAlpha;
    float repoOldRadius;
    float repoEndTheta;
    float repoEndAlpha;
    float repoDeltaTheta;
    float repoDeltaAlpha;
    float repoDeltaRadius;
    int frame;
};

#endif // !CAMERA_H
