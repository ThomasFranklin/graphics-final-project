#pragma once

#ifndef LAB471_VERTEX_H_INCLUDED
#define LAB471_VERTEX_H_INCLUDED

#include <glad/glad.h>

class Vertex {

public:
    GLfloat x;
    GLfloat y;
    GLfloat z;

};

#endif // LAB471_VERTEX_H_INCLUDED#pragma once
