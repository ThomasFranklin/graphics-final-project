#pragma once

#ifndef CHESS_PIECES_H
#define CHESS_PIECES_H

#include "MatrixStack.h"
#include "Material.h"
#include "Shape.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <map>

enum class Color {Gold, Silver};
enum class Rank {PAWN, ROOK, KNIGHT, BISHOP, QUEEN, KING};

class ChessPiece {
public:
    ChessPiece() {};
    ~ChessPiece() {};

    bool inPlay;
    int row;
    int col;
    Rank pieceClass;
    Color color;


    void virtual draw(std::shared_ptr<MatrixStack> M, std::shared_ptr<Program> prog) {};
    void virtual update(bool isKeyframe) {};

    void setupPieceMovementAnimation(int newRow, int newCol);
    bool updatePieceMovement();

    void init(
        int row,
        int col,
        Color color,
        std::shared_ptr<std::map<std::string, std::shared_ptr<Shape>>> shapes);

protected:
    std::shared_ptr<std::map<std::string, std::shared_ptr<Shape>>> shapes;
    float curXDelta;
    float curYDelta;
    float curZDelta;

private:
    int frame;
    int totFrames;
    int destRow;
    int destCol;
    float jumpHeight;

    float xDelta;
    float zDelta;
};

class Pawn : public ChessPiece {
public:
    Pawn();
    ~Pawn() {};

    bool hasMoved;

    void virtual draw(std::shared_ptr<MatrixStack> M, std::shared_ptr<Program> prog);
    void virtual update(bool isKeyframe);

private:
    float pos = 0;
    int frame = 0;
    int delayCounter = 0;
    bool isDelayed = true;
};

class Rook : public ChessPiece {
public:
    Rook();
    ~Rook() {};

    void  virtual draw(std::shared_ptr<MatrixStack> M, std::shared_ptr<Program> prog);
    void  virtual update(bool isKeyframe);

private:
    float pos = 0;
    int direction = 1;
    float rTheta = 0;
};

class Knight : public ChessPiece {
public:
    Knight();
    ~Knight() {};

    void virtual draw(std::shared_ptr<MatrixStack> M, std::shared_ptr<Program> prog);
    void virtual update(bool isKeyframe);

private:
    float pos = 0;
    float rTheta = 0;
};

class Bishop : public ChessPiece {
public:
    Bishop();
    ~Bishop() {};

    void virtual draw(std::shared_ptr<MatrixStack> M, std::shared_ptr<Program> prog);
    void virtual update(bool isKeyframe);

private:
    float pos = 0;
    float rTheta = 0;
};

class Queen : public ChessPiece {
public:
    Queen();
    ~Queen() {};

    void virtual draw(std::shared_ptr<MatrixStack> M, std::shared_ptr<Program> prog);
    void virtual update(bool isKeyframe);

private:
    float rTheta = 0;
};

class King : public ChessPiece {
public:
    King();
    ~King() {};

    void virtual draw(std::shared_ptr<MatrixStack> M, std::shared_ptr<Program> prog);
    void virtual update(bool isKeyframe);

private:
    float rTheta = 0;
};
#endif