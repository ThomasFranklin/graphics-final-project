#pragma once

#ifndef GAME_BOARD_H

#define GAME_BOARD_H

#include "ChessPieces.h"
#include "Material.h"
#include "CaptureAnimation.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include <queue>

#define BOARD_SIZE 8

class Direction {
public:
    Direction() = default;
    ~Direction() = default;

    float pos;
    int dir;
};

class GameBoard {

public:
    static const int SQUARE_UP = 1;
    static const int SQUARE_DOWN = -1;
    static const int SQUARE_CONST = 0;
    static const int DIRECTION_LEFT = -1;
    static const int DIRECTION_RIGHT = 1;
    static const int DIRECTION_UP = 1;
    static const int DIRECTION_DOWN = -1;
    static const int NO_SELECTION = -1;

    GameBoard();
    ~GameBoard();

    void init(std::shared_ptr<std::map<std::string, std::shared_ptr<Shape>>> shapes, CaptureAnimation *captureAnim);
    void draw(std::shared_ptr<MatrixStack> M, std::shared_ptr<Program> prog, bool isKeyframe);
    void changeSelectedPiece(const int direction, Color color);
    void changeSelectedSquare(const int direction);
    void resetSelectedPiece(const bool hardReset);
    void resetEligibleMoves();
    bool canMoveSelectedPiece();
    void prepareToMoveSelectedPiece();
    void beginMovingSelectedPiece();
    bool moveSelectedPiece();
    bool endMovingSelectedPiece();

private:
    // Materials for the board and pieces
    Material black;
    Material white;
    Material green;
    Material gold;
    Material silver;

    std::shared_ptr<std::map<std::string, std::shared_ptr<Shape>>> shapes;

    // Chess piece positions, board elevation, and potential movement grids
    ChessPiece* board[BOARD_SIZE][BOARD_SIZE];
    Direction boardPositions[BOARD_SIZE][BOARD_SIZE];
    bool pieceMovement[BOARD_SIZE][BOARD_SIZE];

    std::vector<ChessPiece*> goldPieces;
    std::vector<ChessPiece*> silverPieces;

    int selectedPiece;
    Color selectedPieceColor;
    int selectedSquare;
    std::vector<std::pair<int, int>> eligibleMoves;
    std::queue<std::pair<int, int>> path;
    std::pair<int, int> start;
    std::pair<int, int> destination;

    CaptureAnimation *captureAnimation;

    // helper functions
    void initPieces();
    void initMaterials();

    void populatePawnMoves(Pawn* pawn);
    void populateRookMoves(Rook* rook);
    void populateKnightMoves(Knight* knight);
    void populateBishopMoves(Bishop* bishop);
    void populateQueenMoves(Queen* queen);
    void populateKingMoves(King* king);
    void rangedMoveHelper(int row, int col, int rowDelta, int colDelta, Color color, bool limit);
    void lMoveHelper(int newRow, int newCol, Color color);
    void buildPath();
    void buildStandardPath(int startRow, int startCol, int destRow, int destCol);

    bool isWithinBoard(int row, int col);
    void populateEligibleMoves();
    ChessPiece* getSelectedPiece();

};

#endif // !GAME_BOARD_H