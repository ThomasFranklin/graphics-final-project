/*
 * Final Project
 * By Thomas Franklin
 */

#include <iostream>
#include <glad/glad.h>
#include <string.h>

#include "GLSL.h"
#include "Program.h"
#include "Shape.h"
#include "MatrixStack.h"
#include "WindowManager.h"
#include "ChessPieces.h"
#include "GameBoard.h"
#include "GameStateMachine.h"
#include "Camera.h"
#include "Constants.h"
#include "CaptureAnimation.h"


 // value_ptr for glm
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define MOVEMENT_DELTA .5f

using namespace std;
using namespace glm;
using namespace constants;

class Application : public EventCallbacks {

public:

    WindowManager * windowManager = nullptr;

    int windowWidth;
    int windowHeight;

    // Our shader program
    shared_ptr<Program> phongProg;
    shared_ptr<Program> partProg;

    // Shapes to be used (from obj file)
    shared_ptr<map<string, shared_ptr<Shape>>> shapes;

    // the main game board and state machine
    GameStateMachine stateMachine;
    GameBoard board;

    GLfloat lightPos[3] = {-10, 10, -8};
    GLfloat lightColor[3] = {1, 1, 1};

    Camera camera;
    CaptureAnimation captureAnimation;

    bool isLatRotating = false;
    bool isVertRotating = false;
    bool isZooming = false;

    int latRotateDir = 1;
    int vertRotateDir = 1;
    int zoomDir = 1;

    double lastRender;

    void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
            glfwSetWindowShouldClose(window, GL_TRUE);
        } else if (key == GLFW_KEY_A) {
            if (action == GLFW_PRESS) {
                isLatRotating = true;
                latRotateDir = 1;
            } else if (action == GLFW_RELEASE) {
                isLatRotating = false;
            }
        } else if (key == GLFW_KEY_D) {
            if (action == GLFW_PRESS) {
                isLatRotating = true;
                latRotateDir = -1;
            } else if (action == GLFW_RELEASE) {
                isLatRotating = false;
            }
        } else if (key == GLFW_KEY_W) {
            if (action == GLFW_PRESS) {
                isVertRotating = true;
                vertRotateDir = -1;
            } else if (action == GLFW_RELEASE) {
                isVertRotating = false;
            }
        } else if (key == GLFW_KEY_S) {
            if (action == GLFW_PRESS) {
                isVertRotating = true;
                vertRotateDir = 1;
            } else if (action == GLFW_RELEASE) {
                isVertRotating = false;
            }
        } else if (key == GLFW_KEY_Q) {
            if (action == GLFW_PRESS) {
                isZooming = true;
                zoomDir = -1;
            } else if (action == GLFW_RELEASE) {
                isZooming = false;
            }
        } else if (key == GLFW_KEY_E) {
            if (action == GLFW_PRESS) {
                isZooming = true;
                zoomDir = 1;
            } else if (action == GLFW_RELEASE) {
                isZooming = false;
            }
        } else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
            stateMachine.changeSelectedPiece(GameBoard::DIRECTION_LEFT);
        } else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
            stateMachine.changeSelectedPiece(GameBoard::DIRECTION_RIGHT);
        } else if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
            stateMachine.changeSelectedSquare(GameBoard::DIRECTION_UP);
        } else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
            stateMachine.changeSelectedSquare(GameBoard::DIRECTION_DOWN);
        } else if ((key == GLFW_KEY_BACKSPACE || key == GLFW_KEY_DELETE) && action == GLFW_PRESS) {
            stateMachine.resetSelection();
        } else if (key == GLFW_KEY_ENTER && action == GLFW_PRESS) {
            stateMachine.movePiece();
        }
    }

    void mouseCallback(GLFWwindow *window, int button, int action, int mods) {
        double posX, posY;

        if (action == GLFW_PRESS) {
            glfwGetCursorPos(window, &posX, &posY);
            cout << "Pos X " << posX << " Pos Y " << posY << endl;
        }
    }

    void cursorCallback(GLFWwindow *window, double xPos, double yPos) {
        // EMPTY
    }

    void resizeCallback(GLFWwindow *window, int width, int height) {
        windowWidth = width;
        windowHeight = height;

        glViewport(0, 0, width, height);
    }

    void init(const string& resourceDirectory) {
        GLSL::checkVersion();

        // Set background color.
        glClearColor(.12f, .34f, .56f, 1.0f);
        // Enable z-buffer test.
        glEnable(GL_DEPTH_TEST);
        CHECKED_GL_CALL(glEnable(GL_BLEND));
        CHECKED_GL_CALL(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
        CHECKED_GL_CALL(glPointSize(14.0f));

        partProg = make_shared<Program>();
        partProg->setVerbose(true);
        partProg->setShaderNames(
            resourceDirectory + "/particle_vert.glsl",
            resourceDirectory + "/particle_frag.glsl");
        if (!partProg->init()) {
            std::cerr << "One or more shaders failed to compile... exiting!" << std::endl;
            exit(1);
        }
        partProg->addUniform("P");
        partProg->addUniform("M");
        partProg->addUniform("V");
        partProg->addUniform("alphaTexture");
        partProg->addAttribute("vertPos");

        // Initialize the GLSL program
        phongProg = make_shared<Program>();
        phongProg->setVerbose(true);
        phongProg->setShaderNames(resourceDirectory + "/phong_vert.glsl", resourceDirectory + "/phong_frag.glsl");
        phongProg->init();
        phongProg->addUniform("P");
        phongProg->addUniform("V");
        phongProg->addUniform("M");
        phongProg->addUniform("lightPos");
        phongProg->addUniform("kd");
        phongProg->addUniform("lightColor");
        phongProg->addUniform("ks");
        phongProg->addUniform("shininess");
        phongProg->addUniform("ambient");
        phongProg->addAttribute("vertPos");
        phongProg->addAttribute("vertNor");

        camera.init();

        lastRender = glfwGetTime();
    }

    void initGeom(const string& resourceDirectory) {
        shapes = make_shared<map<string, shared_ptr<Shape>>>();

        // Initialize meshes for all the shapes.
        shared_ptr<Shape> cube = make_shared<Shape>();
        cube->loadMesh(resourceDirectory + SHAPE_CUBE);
        cube->resize();
        cube->init();
        shapes->insert(make_pair(SHAPE_CUBE, cube));

        shared_ptr<Shape> sphere = make_shared<Shape>();
        sphere->loadMesh(resourceDirectory + SHAPE_SPHERE);
        sphere->resize();
        sphere->init();
        shapes->insert(make_pair(SHAPE_SPHERE, sphere));

        shared_ptr<Shape> cone = make_shared<Shape>();
        cone->loadMesh(resourceDirectory + SHAPE_CONE);
        cone->resize();
        cone->init();
        shapes->insert(make_pair(SHAPE_CONE, cone));

        shared_ptr<Shape> cylinder = make_shared<Shape>();
        cylinder->loadMesh(resourceDirectory + SHAPE_CYLINDER);
        cylinder->resize();
        cylinder->init();
        shapes->insert(make_pair(SHAPE_CYLINDER, cylinder));

        shared_ptr<Shape> diamond = make_shared<Shape>();
        diamond->loadMesh(resourceDirectory + SHAPE_DIAMOND);
        diamond->resize();
        diamond->init();
        shapes->insert(make_pair(SHAPE_DIAMOND, diamond));

        shared_ptr<Shape> spring = make_shared<Shape>();
        spring->loadMesh(resourceDirectory + SHAPE_SPRING);
        spring->resize();
        spring->init();
        shapes->insert(make_pair(SHAPE_SPRING, spring));

        shared_ptr<Shape> cross = make_shared<Shape>();
        cross->loadMesh(resourceDirectory + SHAPE_CROSS);
        cross->resize();
        cross->init();
        shapes->insert(make_pair(SHAPE_CROSS, cross));

        shared_ptr<Shape> shell = make_shared<Shape>();
        shell->loadMesh(resourceDirectory + SHAPE_SHELL);
        shell->resize();
        shell->init();
        shapes->insert(make_pair(SHAPE_SHELL, shell));

        captureAnimation.initSystem(resourceDirectory);
        board.init(shapes, &captureAnimation);
        stateMachine.init(&board, &camera);
    }

    void render() {
        // Get current frame buffer size.
        int width, height;
        glfwGetFramebufferSize(windowManager->getHandle(), &width, &height);
        glViewport(0, 0, width, height);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        // Clear framebuffer.
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        float aspect = width / (float) height;

        // Create the matrix stacks
        auto P = make_shared<MatrixStack>();
        auto V = make_shared<MatrixStack>();
        auto M = make_shared<MatrixStack>();

        // Apply perspective projection.
        P->pushMatrix();
        P->perspective(45.0f, aspect, 0.01f, 100.0f);

        double currentTime = glfwGetTime();
        double renderDelta = currentTime - lastRender;
        bool isKeyframe = renderDelta >= FRAME_TIME;

        if (isKeyframe) {
            float deltaTheta = 0;
            float deltaAlpha = 0;
            float deltaZoom = 0;
            if (isVertRotating) {
                deltaTheta = vertRotateDir * MOVEMENT_DELTA * 2;
            }

            if (isLatRotating) {
                deltaAlpha = latRotateDir * MOVEMENT_DELTA * 2;
            }

            if (isZooming) {
                deltaZoom = zoomDir * MOVEMENT_DELTA;
            }

            camera.rotateCamera(deltaTheta, deltaAlpha, deltaZoom);

            stateMachine.transition();

            lastRender = currentTime;
        }


        camera.applyCamera(V);

        phongProg->bind();

        glUniformMatrix4fv(phongProg->getUniform("P"), 1, GL_FALSE, value_ptr(P->topMatrix()));
        glUniformMatrix4fv(phongProg->getUniform("V"), 1, GL_FALSE, value_ptr(V->topMatrix()));

        glUniform3fv(phongProg->getUniform("lightPos"), 1, lightPos);
        glUniform3fv(phongProg->getUniform("lightColor"), 1, lightColor);

        board.draw(M, phongProg, isKeyframe);

        phongProg->unbind();

        partProg->bind();
        captureAnimation.render(camera, P, M, V, partProg, isKeyframe);
        partProg->unbind();


        // Pop matrix stacks.
        M->popMatrix();
        P->popMatrix();
    }
};

int main(int argc, char *argv[]) {
    // Where the resources are loaded from
    string resourceDir = RESOURCE_DIR;

    Application *application = new Application();

    WindowManager *windowManager = new WindowManager();
    windowManager->init();
    windowManager->setEventCallbacks(application);
    application->windowManager = windowManager;

    application->init(resourceDir);
    application->initGeom(resourceDir);
    application->stateMachine.resumeGame();

    // Loop until the user closes the window.
    while (!glfwWindowShouldClose(windowManager->getHandle())) {
        // Render scene.
        application->render();

        // Swap front and back buffers.
        glfwSwapBuffers(windowManager->getHandle());
        // Poll for and process events.
        glfwPollEvents();
    }

    // Quit program.
    windowManager->shutdown();
    return 0;
}
