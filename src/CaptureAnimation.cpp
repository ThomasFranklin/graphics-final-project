#include "CaptureAnimation.h"

#include <glad/glad.h>
#include <algorithm>
#include "GLSL.h"

using namespace std;

void CaptureAnimation::initSystem(const string& resourceDirectory) {
    initTex(resourceDirectory);
    initParticles();
    initGeom();
}

// Code to load in the three textures
void CaptureAnimation::initTex(const string& resourceDirectory) {
    texture = make_shared<Texture>();
    texture->setFilename(resourceDirectory + "/alpha.bmp");
    texture->init();
    texture->setUnit(0);
    texture->setWrapModes(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
}

void CaptureAnimation::initParticles() {
    for (int i = 0; i < NUM_PARTICLES; ++i) {
        auto particle = make_shared<Particle>();
        particles.push_back(particle);
        particle->rebirth(RUN_TIME, 0, 0, 1);
    }
}

void CaptureAnimation::initGeom() {
    // generate the VAO
    CHECKED_GL_CALL(glGenVertexArrays(1, &VertexArrayID));
    CHECKED_GL_CALL(glBindVertexArray(VertexArrayID));

    // generate vertex buffer to hand off to OGL - using instancing
    CHECKED_GL_CALL(glGenBuffers(1, &pointsbuffer));
    // set the current state to focus on our vertex buffer
    CHECKED_GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, pointsbuffer));
    // actually memcopy the data - only do this once
    CHECKED_GL_CALL(glBufferData(GL_ARRAY_BUFFER, sizeof(points), NULL, GL_STREAM_DRAW));

    CHECKED_GL_CALL(glGenBuffers(1, &colorbuffer));
    // set the current state to focus on our vertex buffer
    CHECKED_GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, colorbuffer));
    // actually memcopy the data - only do this once
    CHECKED_GL_CALL(glBufferData(GL_ARRAY_BUFFER, sizeof(pointColors), NULL, GL_STREAM_DRAW));
}

void CaptureAnimation::startSystem(ChessPiece *capturedPiece, bool currentPieceIsKnight) {
    animationRunning = true;
    animationDelayed = true;
    frame = 0;
    this->capturedPiece = capturedPiece;
    this->currentPieceIsKnight = currentPieceIsKnight;

    for (auto particle : particles) {
        particle->rebirth(
            RUN_TIME,
            2 * capturedPiece->col,
            -2 * capturedPiece->row,
            capturedPiece->color == Color::Gold ? 1 : 0);
    }
}

void CaptureAnimation::render(
        Camera &camera,
        shared_ptr<MatrixStack> P,
        shared_ptr<MatrixStack> M,
        shared_ptr<MatrixStack> V,
        shared_ptr<Program> prog,
        bool isKeyframe) {

    if (animationRunning) {

        if (isKeyframe) {
            if (animationDelayed) {
                frame++;

                int modifiedDelayTime = currentPieceIsKnight ? DELAY_TIME * 3 : DELAY_TIME;
                if (frame > modifiedDelayTime) {
                    animationDelayed = false;
                    frame = 0;
                }
                return;
            }

            updateParticles(camera);
            updateGeom();
            if (frame > PIECE_REMOVE_TIME) {
                capturedPiece->inPlay = false;
            }

            frame++;
        }

        texture->bind(prog->getUniform("alphaTexture"));
        CHECKED_GL_CALL(glUniformMatrix4fv(prog->getUniform("P"), 1, GL_FALSE, value_ptr(P->topMatrix())));
        CHECKED_GL_CALL(glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix())));
        CHECKED_GL_CALL(glUniformMatrix4fv(prog->getUniform("V"), 1, GL_FALSE, value_ptr(V->topMatrix())));

        CHECKED_GL_CALL(glEnableVertexAttribArray(0));
        CHECKED_GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, pointsbuffer));
        CHECKED_GL_CALL(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0));

        CHECKED_GL_CALL(glEnableVertexAttribArray(1));
        CHECKED_GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, colorbuffer));
        CHECKED_GL_CALL(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0));

        CHECKED_GL_CALL(glVertexAttribDivisor(0, 1));
        CHECKED_GL_CALL(glVertexAttribDivisor(1, 1));
        // Draw the points !
        CHECKED_GL_CALL(glDrawArraysInstanced(GL_POINTS, 0, 1, NUM_PARTICLES));

        CHECKED_GL_CALL(glVertexAttribDivisor(0, 0));
        CHECKED_GL_CALL(glVertexAttribDivisor(1, 0));
        CHECKED_GL_CALL(glDisableVertexAttribArray(0));
        CHECKED_GL_CALL(glDisableVertexAttribArray(1));

        if (frame > RUN_TIME) {
            animationRunning = false;
        }
    }
}

void CaptureAnimation::updateParticles(Camera &camera) {
    // update the particles
    for (auto particle : particles) {
        particle->update(frame);
    }

    // Sort the particles by Z
    auto temp = make_shared<MatrixStack>();
    camera.applyCamera(temp);

    ParticleSorter sorter;
    sorter.C = temp->topMatrix();
    sort(particles.begin(), particles.end(), sorter);
}

void CaptureAnimation::updateGeom() {
    vec3 pos;
    vec4 col;

    // go through all the particles and update the CPU buffer
    for (int i = 0; i < NUM_PARTICLES; i++) {
        pos = particles[i]->getPosition();
        col = particles[i]->getColor();
        points[i * 3 + 0] = pos.x;
        points[i * 3 + 1] = pos.y;
        points[i * 3 + 2] = pos.z;
        pointColors[i * 4 + 0] = col.r;
        pointColors[i * 4 + 1] = col.g;
        pointColors[i * 4 + 2] = col.b;
        pointColors[i * 4 + 3] = col.a;
    }

    // update the GPU data
    CHECKED_GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, pointsbuffer));
    CHECKED_GL_CALL(glBufferData(GL_ARRAY_BUFFER, sizeof(points), NULL, GL_STREAM_DRAW));
    CHECKED_GL_CALL(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * NUM_PARTICLES * 3, points));

    CHECKED_GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, colorbuffer));
    CHECKED_GL_CALL(glBufferData(GL_ARRAY_BUFFER, sizeof(pointColors), NULL, GL_STREAM_DRAW));
    CHECKED_GL_CALL(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * NUM_PARTICLES * 4, pointColors));

    CHECKED_GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, 0));
}