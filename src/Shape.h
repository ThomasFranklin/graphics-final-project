
#pragma once

#ifndef LAB471_SHAPE_H_INCLUDED
#define LAB471_SHAPE_H_INCLUDED

#include <string>
#include <vector>
#include <memory>
#include "Vertex.h"

#define SHAPE_CUBE "cube.obj"
#define SHAPE_SPHERE "sphere.obj"
#define SHAPE_CONE "cone.obj"
#define SHAPE_CYLINDER "cylinder.obj"
#define SHAPE_DIAMOND "diamond.obj"
#define SHAPE_SPRING "spring.obj"
#define SHAPE_CROSS "cross.obj"
#define SHAPE_SHELL "shell.obj"

class Program;

class Shape {

public:

    void loadMesh(const std::string &meshName);
    void init();
    void resize();
    void draw(const std::shared_ptr<Program> prog) const;

private:

    Vertex cross(Vertex v1, Vertex v2);
    GLfloat length(Vertex vert);
    void calculateNormals();

    std::vector<unsigned int> eleBuf;
    std::vector<float> posBuf;
    std::vector<float> norBuf;
    std::vector<float> texBuf;

    unsigned int eleBufID = 0;
    unsigned int posBufID = 0;
    unsigned int norBufID = 0;
    unsigned int texBufID = 0;
    unsigned int vaoID = 0;

};

#endif // LAB471_SHAPE_H_INCLUDED
