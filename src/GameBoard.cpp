#include "GameBoard.h"
#include "Program.h"

#define INIT_SQUARE_POS 0
#define MAX_SQUARE_POS 1
#define SQUARE_DELTA 0.05f
#define NUM_PIECES 16

using namespace std;
using namespace glm;

GameBoard::GameBoard() {}

GameBoard::~GameBoard() {
    for (auto &chessPiece : goldPieces) {
        delete chessPiece;
    }

    for (auto &chessPiece : silverPieces) {
        delete chessPiece;
    }
}

void GameBoard::init(shared_ptr<map<string, shared_ptr<Shape>>> shapes, CaptureAnimation *captureAnim) {
    this->shapes = shapes;
    this->captureAnimation = captureAnim;

    initPieces();
    initMaterials();
}

void GameBoard::initPieces() {
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0; col < BOARD_SIZE; col++) {
            board[row][col] = nullptr;

            boardPositions[row][col].pos = INIT_SQUARE_POS;
            boardPositions[row][col].dir = SQUARE_CONST;

            pieceMovement[row][col] = false;
        }
    }

    // Initialize board by column
    for (int col = 0; col < BOARD_SIZE; col++) {
        ChessPiece *goldBackRow;
        ChessPiece *silverBackRow;
        ChessPiece *goldPawn = new Pawn();
        ChessPiece *silverPawn = new Pawn();

        switch (col) {
            case 0:
            case 7:
                goldBackRow = new Rook();
                silverBackRow = new Rook();
                break;
            case 1:
            case 6:
                goldBackRow = new Knight();
                silverBackRow = new Knight();
                break;
            case 2:
            case 5:
                goldBackRow = new Bishop();
                silverBackRow = new Bishop();
                break;
            case 3:
                goldBackRow = new King();
                silverBackRow = new King();
                break;
            case 4:
                goldBackRow = new Queen();
                silverBackRow = new Queen();
                break;
            default:
                continue;
        }

        goldBackRow->init(0, col, Color::Gold, shapes);
        board[0][col] = goldBackRow;
        goldPieces.push_back(goldBackRow);

        goldPawn->init(1, col, Color::Gold, shapes);
        board[1][col] = goldPawn;
        goldPieces.push_back(goldPawn);

        silverBackRow->init(7, col, Color::Silver, shapes);
        board[7][col] = silverBackRow;
        silverPieces.push_back(silverBackRow);

        silverPawn->init(6, col, Color::Silver, shapes);
        board[6][col] = silverPawn;
        silverPieces.push_back(silverPawn);

        // DEBUG: places a piece in the middle of the board
        /*
        if (col == 4) {
            ChessPiece *debug = new King();
            debug->init(4, col, Color::Gold, shapes);
            board[4][col] = debug;
            goldPieces.push_back(debug);
        }
        */
    }

    selectedPiece = NO_SELECTION;
    selectedSquare = NO_SELECTION;
}

void GameBoard::initMaterials() {
    black.kd[0] = 0.08275f;
    black.kd[1] = 0.07f;
    black.kd[2] = 0.02525f;
    black.ks[0] = 0.132741f;
    black.ks[1] = 0.128634f;
    black.ks[2] = 0.146435f;
    black.shininess = 0.5f;
    black.ambient[0] = 0.05375f;
    black.ambient[1] = 0.05f;
    black.ambient[2] = 0.06625f;

    white.kd[0] = 0.829f;
    white.kd[1] = 0.829f;
    white.kd[2] = 0.829f;
    white.ks[0] = 0.296648f;
    white.ks[1] = 0.296648f;
    white.ks[2] = 0.296648f;
    white.shininess = 11.264f;
    white.ambient[0] = 0.25f;
    white.ambient[1] = 0.20725f;
    white.ambient[2] = 0.20725f;

    green.kd[0] = 0.54f;
    green.kd[1] = 0.89f;
    green.kd[2] = 0.63f;
    green.ks[0] = 0.316228f;
    green.ks[1] = 0.316228f;
    green.ks[2] = 0.316228f;
    green.shininess = 12.8f;
    green.ambient[0] = 0.135f;
    green.ambient[1] = 0.2225f;
    green.ambient[2] = 0.1575f;

    gold.kd[0] = 0.75164f;
    gold.kd[1] = 0.60648f;
    gold.kd[2] = 0.22648f;
    gold.ks[0] = 0.628281f;
    gold.ks[1] = 0.555802f;
    gold.ks[2] = 0.366065f;
    gold.shininess = 51.2f;
    gold.ambient[0] = 0.24725f;
    gold.ambient[1] = 0.1995f;
    gold.ambient[2] = 0.0745f;

    silver.kd[0] = 0.50754f;
    silver.kd[1] = 0.50754f;
    silver.kd[2] = 0.50754f;
    silver.ks[0] = 0.508273f;
    silver.ks[1] = 0.508273f;
    silver.ks[2] = 0.508273f;
    silver.shininess = 51.2f;
    silver.ambient[0] = 0.19225f;
    silver.ambient[1] = 0.19225f;
    silver.ambient[2] = 0.19225f;
}

void GameBoard::draw(shared_ptr<MatrixStack> M, shared_ptr<Program> prog, bool isKeyframe) {
    M->pushMatrix();
    M->loadIdentity();
    M->translate(vec3(-BOARD_SIZE, -3, 0));

    // update positions
    if (isKeyframe) {
        for (int row = 0; row < BOARD_SIZE; row++) {
            for (int col = 0; col < BOARD_SIZE; col++) {
                if (boardPositions[row][col].dir != SQUARE_CONST) {
                    boardPositions[row][col].pos += SQUARE_DELTA * boardPositions[row][col].dir;
                    if (boardPositions[row][col].pos < INIT_SQUARE_POS) {
                        boardPositions[row][col].pos = INIT_SQUARE_POS;
                        boardPositions[row][col].dir = SQUARE_CONST;
                    } else if (boardPositions[row][col].pos > MAX_SQUARE_POS) {
                        boardPositions[row][col].pos = MAX_SQUARE_POS;
                        boardPositions[row][col].dir = SQUARE_CONST;
                    }
                }
            }
        }
    }

    // draw all the eligible movement tiles
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0; col < BOARD_SIZE; col++) {
            if (pieceMovement[row][col]) {
                glUniform3fv(prog->getUniform("kd"), 1, green.kd);
                glUniform3fv(prog->getUniform("ks"), 1, green.ks);
                glUniform1f(prog->getUniform("shininess"), green.shininess);
                glUniform3fv(prog->getUniform("ambient"), 1, green.ambient);

                M->pushMatrix();
                M->translate(vec3(2 * col, boardPositions[row][col].pos, -2 * row));
                glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
                shapes->at(SHAPE_CUBE)->draw(prog);
                M->popMatrix();
            }
        }
    }

    // draw all the white tiles
    glUniform3fv(prog->getUniform("kd"), 1, white.kd);
    glUniform3fv(prog->getUniform("ks"), 1, white.ks);
    glUniform1f(prog->getUniform("shininess"), white.shininess);
    glUniform3fv(prog->getUniform("ambient"), 1, white.ambient);
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = row % 2; col < BOARD_SIZE; col += 2) {
            if (!pieceMovement[row][col]) {
                M->pushMatrix();
                M->translate(vec3(2 * col, boardPositions[row][col].pos, -2 * row));
                glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
                shapes->at(SHAPE_CUBE)->draw(prog);
                M->popMatrix();
            }
        }
    }

    // draw all the black tiles
    glUniform3fv(prog->getUniform("kd"), 1, black.kd);
    glUniform3fv(prog->getUniform("ks"), 1, black.ks);
    glUniform1f(prog->getUniform("shininess"), black.shininess);
    glUniform3fv(prog->getUniform("ambient"), 1, black.ambient);
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = (row + 1) % 2; col < BOARD_SIZE; col += 2) {
            if (!pieceMovement[row][col]) {
                M->pushMatrix();
                M->translate(vec3(2 * col, boardPositions[row][col].pos, -2 * row));
                glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
                shapes->at(SHAPE_CUBE)->draw(prog);
                M->popMatrix();
            }
        }
    }

    glUniform3fv(prog->getUniform("kd"), 1, gold.kd);
    glUniform3fv(prog->getUniform("ks"), 1, gold.ks);
    glUniform1f(prog->getUniform("shininess"), gold.shininess);
    glUniform3fv(prog->getUniform("ambient"), 1, gold.ambient);
    for (auto &chessPiece : goldPieces) {
        if (chessPiece->inPlay) {
            M->pushMatrix();
            M->translate(vec3(0, boardPositions[chessPiece->row][chessPiece->col].pos, 0));
            chessPiece->draw(M, prog);
            chessPiece->update(isKeyframe);
            M->popMatrix();
        }
    }

    glUniform3fv(prog->getUniform("kd"), 1, silver.kd);
    glUniform3fv(prog->getUniform("ks"), 1, silver.ks);
    glUniform1f(prog->getUniform("shininess"), silver.shininess);
    glUniform3fv(prog->getUniform("ambient"), 1, silver.ambient);
    for (auto &chessPiece : silverPieces) {
        if (chessPiece->inPlay) {
            M->pushMatrix();
            M->translate(vec3(0, boardPositions[chessPiece->row][chessPiece->col].pos, 0));
            chessPiece->draw(M, prog);
            chessPiece->update(isKeyframe);
            M->popMatrix();
        }
    }
}

void GameBoard::changeSelectedPiece(const int direction, Color color) {
    // reset any lingering eligible moves
    resetEligibleMoves();

    // lower the currently selected square
    resetSelectedPiece(false);

    ChessPiece* currentPiece;
    selectedPieceColor = color;

    // change selection
    do {
        selectedPiece += direction;
        if (selectedPiece < 0) {
            selectedPiece = NUM_PIECES - 1;
        } else if (selectedPiece >= NUM_PIECES) {
            selectedPiece = 0;
        }

        currentPiece = getSelectedPiece();
    } while (!(currentPiece->inPlay));

    // raise the currently selected square
    boardPositions[currentPiece->row][currentPiece->col].dir = SQUARE_UP;

    // last, populate eligible moves for square highlighting
    populateEligibleMoves();
}

void GameBoard::changeSelectedSquare(const int direction) {
    if (selectedPiece == NO_SELECTION || eligibleMoves.size() == 0) {
        return;
    }

    if (selectedSquare == NO_SELECTION) {
        if (direction == DIRECTION_UP) {
            selectedSquare = 0;
        } else {
            selectedSquare = eligibleMoves.size() - 1;
        }
    } else {
        pair<int, int> square = eligibleMoves.at(selectedSquare);
        boardPositions[square.first][square.second].dir = SQUARE_DOWN;
        selectedSquare += direction;

        if (selectedSquare < 0) {
            selectedSquare = eligibleMoves.size() - 1;
        } else if (selectedSquare >= eligibleMoves.size()) {
            selectedSquare = 0;
        }
    }

    pair<int, int> square = eligibleMoves.at(selectedSquare);
    boardPositions[square.first][square.second].dir = SQUARE_UP;
}

void GameBoard::resetEligibleMoves() {
    if (selectedSquare != NO_SELECTION) {
        pair<int, int> square = eligibleMoves.at(selectedSquare);
        boardPositions[square.first][square.second].dir = SQUARE_DOWN;
        selectedSquare = NO_SELECTION;
    }

    for (auto &location : eligibleMoves) {
        pieceMovement[location.first][location.second] = false;
    }

    eligibleMoves.clear();
}

ChessPiece* GameBoard::getSelectedPiece() {
    if (selectedPiece != NO_SELECTION) {
        return selectedPieceColor == Color::Gold
            ? goldPieces.at(selectedPiece)
            : silverPieces.at(selectedPiece);
    } else {
        return nullptr;
    }
}

void GameBoard::resetSelectedPiece(const bool hardReset) {
    ChessPiece* currentPiece = getSelectedPiece();

    if (currentPiece) {
        boardPositions[currentPiece->row][currentPiece->col].dir = SQUARE_DOWN;
    }

    if (hardReset) {
        selectedPiece = NO_SELECTION;
    }
}

void GameBoard::populateEligibleMoves() {
    if (selectedPiece == NO_SELECTION) {
        return;
    }

    // populate potential moves
    ChessPiece* currentPiece;
    currentPiece = selectedPieceColor == Color::Gold
        ? goldPieces.at(selectedPiece)
        : silverPieces.at(selectedPiece);

    switch (currentPiece->pieceClass) {
        case Rank::PAWN:
            populatePawnMoves(dynamic_cast<Pawn*>(currentPiece));
            break;
        case Rank::ROOK:
            populateRookMoves(dynamic_cast<Rook*>(currentPiece));
            break;
        case Rank::KNIGHT:
            populateKnightMoves(dynamic_cast<Knight*>(currentPiece));
            break;
        case Rank::BISHOP:
            populateBishopMoves(dynamic_cast<Bishop*>(currentPiece));
            break;
        case Rank::QUEEN:
            populateQueenMoves(dynamic_cast<Queen*>(currentPiece));
            break;
        case Rank::KING:
            populateKingMoves(dynamic_cast<King*>(currentPiece));
            break;
    }
}

void GameBoard::populatePawnMoves(Pawn* pawn) {
    int row = pawn->row;
    int col = pawn->col;
    int newRow;
    int newCol;
    int direction = pawn->color == Color::Gold ? 1 : -1;

    // initial 2-space move
    newRow = row + (2 * direction);
    newCol = col;
    if (!pawn->hasMoved && (board[newRow][newCol] == nullptr)) {
        pieceMovement[newRow][newCol] = true;
        eligibleMoves.push_back(make_pair(newRow, newCol));
    }

    // standard 1-space move
    newRow = row + direction;
    newCol = col;
    if (isWithinBoard(newRow, newCol) && (board[newRow][newCol] == nullptr)) {
        pieceMovement[newRow][newCol] = true;
        eligibleMoves.push_back(make_pair(newRow, newCol));
    }

    // diagonal capture move
    newRow = row + direction;
    newCol = col - 1;
    if (isWithinBoard(newRow, newCol)
        && (board[newRow][newCol] != nullptr)
        && (board[newRow][newCol]->color != pawn->color)) {
        pieceMovement[newRow][newCol] = true;
        eligibleMoves.push_back(make_pair(newRow, newCol));
    }

    // diagonal capture move
    newRow = row + direction;
    newCol = col + 1;
    if (isWithinBoard(newRow, newCol)
        && (board[newRow][newCol] != nullptr)
        && (board[newRow][newCol]->color != pawn->color)) {
        pieceMovement[newRow][newCol] = true;
        eligibleMoves.push_back(make_pair(newRow, newCol));
    }
}

void GameBoard::populateRookMoves(Rook* rook) {
    int row = rook->row;
    int col = rook->col;
    Color color = rook->color;

    // move in all lateral directions
    rangedMoveHelper(row, col, 1, 0, color, false);
    rangedMoveHelper(row, col, -1, 0, color, false);
    rangedMoveHelper(row, col, 0, 1, color, false);
    rangedMoveHelper(row, col, 0, -1, color, false);
}

void GameBoard::populateKnightMoves(Knight* knight) {
    int p = 2;
    int q = 1;
    int row = knight->row;
    int col = knight->col;
    Color color = knight->color;

    lMoveHelper(row + p, col + q, color);
    lMoveHelper(row - p, col + q, color);
    lMoveHelper(row + p, col - q, color);
    lMoveHelper(row - p, col - q, color);
    lMoveHelper(row + q, col + p, color);
    lMoveHelper(row - q, col + p, color);
    lMoveHelper(row + q, col - p, color);
    lMoveHelper(row - q, col - p, color);
}

void GameBoard::populateBishopMoves(Bishop* bishop) {
    int row = bishop->row;
    int col = bishop->col;
    Color color = bishop->color;

    // move in all diagonal directions
    rangedMoveHelper(row, col, 1, 1, color, false);
    rangedMoveHelper(row, col, -1, 1, color, false);
    rangedMoveHelper(row, col, 1, -1, color, false);
    rangedMoveHelper(row, col, -1, -1, color, false);
}

void GameBoard::populateQueenMoves(Queen* queen) {
    int row = queen->row;
    int col = queen->col;
    Color color = queen->color;

    // move in all lateral and diagonal directions
    rangedMoveHelper(row, col, 1, 0, color, false);
    rangedMoveHelper(row, col, -1, 0, color, false);
    rangedMoveHelper(row, col, 0, 1, color, false);
    rangedMoveHelper(row, col, 0, -1, color, false);

    rangedMoveHelper(row, col, 1, 1, color, false);
    rangedMoveHelper(row, col, -1, 1, color, false);
    rangedMoveHelper(row, col, 1, -1, color, false);
    rangedMoveHelper(row, col, -1, -1, color, false);
}

void GameBoard::populateKingMoves(King* king) {
    int row = king->row;
    int col = king->col;
    Color color = king->color;

    // move in all lateral directions, one space
    rangedMoveHelper(row, col, 1, 0, color, true);
    rangedMoveHelper(row, col, -1, 0, color, true);
    rangedMoveHelper(row, col, 0, 1, color, true);
    rangedMoveHelper(row, col, 0, -1, color, true);
}

void GameBoard::rangedMoveHelper(int row, int col, int rowDelta, int colDelta, Color color, bool limit) {
    int newRow = row + rowDelta;
    int newCol = col + colDelta;
    while (isWithinBoard(newRow, newCol)
        && ((board[newRow][newCol] == nullptr)
            || (board[newRow][newCol]->color != color))) {
        pieceMovement[newRow][newCol] = true;
        eligibleMoves.push_back(make_pair(newRow, newCol));

        if ((board[newRow][newCol] != nullptr)
            && (board[newRow][newCol]->color != color)
            || limit) {
            break;
        }

        newRow += rowDelta;
        newCol += colDelta;
    }
}

void GameBoard::lMoveHelper(int newRow, int newCol, Color color) {
    if (isWithinBoard(newRow, newCol) && ((board[newRow][newCol] == nullptr)
        || (board[newRow][newCol]->color != color))) {
        pieceMovement[newRow][newCol] = true;
        eligibleMoves.push_back(make_pair(newRow, newCol));
    }
}

bool GameBoard::isWithinBoard(int row, int col) {
    return (row >= 0) && (row < BOARD_SIZE) && (col >= 0) && (col < BOARD_SIZE);
}


bool GameBoard::canMoveSelectedPiece() {
    return !(selectedPiece == NO_SELECTION || selectedSquare == NO_SELECTION);
}

void GameBoard::prepareToMoveSelectedPiece() {
    buildPath();

    ChessPiece* currentPiece = getSelectedPiece();
    start.first = currentPiece->row;
    start.second = currentPiece->col;
    destination.first = eligibleMoves.at(selectedSquare).first;
    destination.second = eligibleMoves.at(selectedSquare).second;

    // reset any lingering eligible moves
    resetEligibleMoves();

    // lower the currently selected square
    resetSelectedPiece(false);
}

void GameBoard::beginMovingSelectedPiece() {
    ChessPiece* currentPiece = getSelectedPiece();

    pair<int, int> step = path.front();
    path.pop();

    currentPiece->setupPieceMovementAnimation(step.first, step.second);

    if (board[step.first][step.second] != nullptr) {
        captureAnimation->startSystem(board[step.first][step.second], currentPiece->pieceClass == Rank::KNIGHT ? true : false);
    }
}

void GameBoard::buildPath() {
    ChessPiece *piece = getSelectedPiece();
    pair<int, int> destination = eligibleMoves.at(selectedSquare);

    if (piece->pieceClass == Rank::KNIGHT) {
        path.push(destination);
    } else {
        buildStandardPath(piece->row, piece->col, destination.first, destination.second);
    }
}

void GameBoard::buildStandardPath(int startRow, int startCol, int destRow, int destCol) {
    int stepRowDelta = 0;
    int stepColDelta = 0;

    int curRow = startRow;
    int curCol = startCol;

    int rowDelta = destRow - startRow;
    int colDelta = destCol - startCol;

    if (rowDelta > 0) {
        stepRowDelta = 1;
    } else if (rowDelta < 0) {
        stepRowDelta = -1;
    }

    if (colDelta > 0) {
        stepColDelta = 1;
    } else if (colDelta < 0) {
        stepColDelta = -1;
    }

    do {
        curRow += stepRowDelta;
        curCol += stepColDelta;
        path.push(make_pair(curRow, curCol));
    } while (curRow != destRow || curCol != destCol);
}

bool GameBoard::moveSelectedPiece() {
    ChessPiece *piece = getSelectedPiece();

    if (piece->updatePieceMovement()) {
        if (path.size() > 0) {
            pair<int, int> step = path.front();
            path.pop();

            piece->setupPieceMovementAnimation(step.first, step.second);

            if (board[step.first][step.second] != nullptr) {
                captureAnimation->startSystem(board[step.first][step.second], piece->pieceClass == Rank::KNIGHT ? true : false);
            }

            return false;
        } else {
            return true;
        }
    }

    return false;
}

bool GameBoard::endMovingSelectedPiece() {
    ChessPiece* currentPiece = getSelectedPiece();
    bool pieceWasKing = false;

    resetSelectedPiece(true);
    int newRow = destination.first;
    int newCol = destination.second;

    currentPiece->row = newRow;
    currentPiece->col = newCol;

    if (board[newRow][newCol] != nullptr) {
        ChessPiece* capturedPiece = board[newRow][newCol];
        capturedPiece->inPlay = false;
        pieceWasKing = capturedPiece->pieceClass == Rank::KING;
    }

    board[start.first][start.second] = nullptr;
    board[newRow][newCol] = currentPiece;

    // any special piece specific logic goes here
    if (currentPiece->pieceClass == Rank::PAWN) {
        dynamic_cast<Pawn*>(currentPiece)->hasMoved = true;
    }

    return pieceWasKing;
}
