#pragma once

#ifndef GAME_STATE_MACHING_H
#define GAME_STATE_MACHINE_H

#include "GameBoard.h"
#include "Camera.h"

enum class State {PLAYER1, PLAYER2, CAM_REPOSITION, MOVE_PIECE, DELAY, GAME_OVER};

class GameStateMachine {

public:
    GameStateMachine();
    ~GameStateMachine();

    void init(GameBoard* board, Camera* camera);

    void pauseGame();
    void resumeGame();
    void endTurn();
    void transition();

    void changeSelectedPiece(int direction);
    void changeSelectedSquare(int direction);
    void resetSelection();

    void movePiece();

private:
    GameBoard* board;
    Camera* camera;
    bool isPaused;
    State state;
    State previousPlayer;

    int delayTimer;
};

#endif // !GAME_STATE_MACHING_H