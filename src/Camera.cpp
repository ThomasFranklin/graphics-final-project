#include "Camera.h"
#include "Constants.h"

using namespace std;
using namespace glm;

const float Camera::GOLD_THETA = -40;
const float Camera::GOLD_ALPHA = 0;
const float Camera::SILVER_THETA = -40;
const float Camera::SILVER_ALPHA = 180;
const float Camera::DEFAULT_RADIUS = 25.f;
const int Camera::FRAMES = constants::FRAME_RATE * 4;

const vec3 Camera::UP = initUp();
const vec3 Camera::LOOK_AT = initLookAt();

const vec3 Camera::initUp() {
    return vec3(0, 1, 0);
}

const vec3 Camera::initLookAt() {
    return vec3(0, -2, -8);
}

void Camera::init() {
    theta = GOLD_THETA;
    alpha = GOLD_ALPHA;
    radius = DEFAULT_RADIUS;

}

void Camera::rotateCamera(const float deltaTheta, const float deltaAlpha, const float deltaZoom) {
    theta += deltaTheta;
    alpha += deltaAlpha;
    radius += deltaZoom;

    // keep theta between -89 and -10 degrees
    theta = max(-89.f, min(-10.f, theta));

    // clamp alpha between 0 and 359 dgrees, preserving rotation
    if (alpha >= 360) {
        alpha = 360 - alpha;
    } else if (alpha < 0) {
        alpha = 360 + alpha;
    }

    // keep radius between 10 and 40 units
    radius = max(10.f, min(40.f, radius));

    updateEyeVec();
}

void Camera::prepareForReposition(const float endTheta, const float endAlpha) {
    frame = 0;
    repoOldTheta = theta;
    repoOldAlpha = alpha;
    repoOldRadius = radius;
    repoEndTheta = endTheta;
    repoEndAlpha = endAlpha;
    repoDeltaTheta = (endTheta - theta);
    repoDeltaAlpha = (endAlpha - alpha);
    repoDeltaRadius = (DEFAULT_RADIUS - radius);
    isRepositioning = true;
}

bool Camera::updateReposition() {
    if (frame <= FRAMES) {
        alpha = -(repoDeltaAlpha / 2) * cos(radians(frame * 180.f / FRAMES)) + min(repoOldAlpha, repoEndAlpha) + (abs(repoDeltaAlpha) / 2);
        theta = -(repoDeltaTheta / 2) * cos(radians(frame * 180.f / FRAMES)) + min(repoOldTheta, repoEndTheta) + (abs(repoDeltaTheta) / 2);
        radius = -(repoDeltaRadius / 2) * cos(radians(frame * 180.f / FRAMES)) + min(repoOldRadius, DEFAULT_RADIUS) + (abs(repoDeltaRadius) / 2);
        updateEyeVec();

        frame++;
        return false;
    } else {
        return true;
    }
}


void Camera::updateEyeVec() {
    vec3 view(
        cos(radians(theta)) * sin(radians(alpha)),
        sin(radians(theta)),
        cos(radians(theta)) * cos(3.14159f - radians(alpha)));

    eye = LOOK_AT - (view * radius);
}

void Camera::applyCamera(shared_ptr<MatrixStack> V) {
    V->lookAt(eye, LOOK_AT, UP);
}
