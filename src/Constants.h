#pragma once

#define RESOURCE_DIR "../resources/"

namespace constants {

    static const int FRAME_RATE = 60;
    static const float FRAME_TIME = .016f;
}