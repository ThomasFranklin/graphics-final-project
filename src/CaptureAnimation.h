#pragma once

#include <glm/gtc/type_ptr.hpp>
#include <vector>

#include "Particle.h"
#include "ChessPieces.h"
#include "Camera.h"
#include "Texture.h"
#include "Program.h"
#include "MatrixStack.h"
#include "Constants.h"

class CaptureAnimation {

public:
    CaptureAnimation() = default;
    ~CaptureAnimation() = default;

    void initSystem(const std::string& resourceDirectory);
    void startSystem(ChessPiece *capturedPiece, bool currentPieceIsKnight);
    void render(
        Camera &camera,
        std::shared_ptr<MatrixStack> P,
        std::shared_ptr<MatrixStack> M,
        std::shared_ptr<MatrixStack> V,
        std::shared_ptr<Program> prog,
        bool isKeyframe);

private:
    static const int NUM_PARTICLES = 500;
    static const int RUN_TIME = constants::FRAME_RATE * 3;
    static const int DELAY_TIME = constants::FRAME_RATE / 4;
    static const int PIECE_REMOVE_TIME = 3;
    // particles
    std::vector<std::shared_ptr<Particle>> particles;

    // CPU array for particles - redundant with particle structure
    // but simple
    GLfloat points[3 * NUM_PARTICLES];
    GLfloat pointColors[4 * NUM_PARTICLES];

    int frame;
    bool animationRunning = false;
    bool animationDelayed = false;
    bool currentPieceIsKnight;

    ChessPiece *capturedPiece;

    GLuint pointsbuffer;
    GLuint colorbuffer;

    // OpenGL handle to texture data
    std::shared_ptr<Texture> texture;

    // Contains vertex information for OpenGL
    GLuint VertexArrayID;

    void initTex(const std::string& resourceDirectory);
    void initParticles();
    void initGeom();

    void updateParticles(Camera &camera);
    void updateGeom();
};

