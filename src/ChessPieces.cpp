#include "ChessPieces.h"

#include "Program.h"
#include "Constants.h"

using namespace glm;
using namespace std;
using namespace constants;

void ChessPiece::init(
        int row,
        int col,
        Color color,
        shared_ptr<map<string, shared_ptr<Shape>>> shapes) {
    this->row = row;
    this->col = col;
    this->color = color;
    this->inPlay = true;
    this->shapes = shapes;

    this->curXDelta = 0;
    this->curYDelta = 0;
    this->curZDelta = 0;
}

void ChessPiece::setupPieceMovementAnimation(int newRow, int newCol) {
    destRow = newRow;
    destCol = newCol;
    frame = 0;
    totFrames = pieceClass == Rank::KNIGHT ? (int) (FRAME_RATE * 1.5) : FRAME_RATE / 2;
    jumpHeight = pieceClass == Rank::KNIGHT ? 4.f : 2.f;

    xDelta = 2 * (newCol - col) / (float) (totFrames - 1);
    zDelta = 2 * (row - newRow) / (float) (totFrames - 1);
}

bool ChessPiece::updatePieceMovement() {
    if (frame > totFrames) {
        row = destRow;
        col = destCol;

        curXDelta = 0;
        curYDelta = 0;
        curZDelta = 0;

        return true;
    }

    frame++;

    curXDelta += xDelta;
    curYDelta = -(float) (jumpHeight / pow(totFrames / 2, 2)) * (float) pow(frame - totFrames / 2, 2) + jumpHeight;
    curZDelta += zDelta;

    return false;
}

#define PAWN_SPHERE_BASE 2.1f
#define PAWN_JUMP_HEIGHT 1
Pawn::Pawn() {
    pieceClass = Rank::PAWN;
    hasMoved = false;
};

void Pawn::draw(shared_ptr<MatrixStack> M, shared_ptr<Program> prog) {
    {
        M->pushMatrix();
        M->translate(vec3(curXDelta, curYDelta, curZDelta));
        M->translate(vec3(2 * col, 1, -2 * row));

        {
            M->pushMatrix();
            M->translate(vec3(0, .05f, 0));
            M->scale(vec3(.7f, .05f, .7f));
            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_CYLINDER)->draw(prog);
            M->popMatrix();
        }

        {
            M->pushMatrix();
            M->translate(vec3(0, 1, 0));
            M->scale(vec3(.7f, .8f, .7f));

            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_CONE)->draw(prog);
            M->popMatrix();
        }

        {
            M->pushMatrix();
            M->translate(vec3(0, PAWN_SPHERE_BASE + pos, 0));
            M->scale(vec3(.3f, .3f, .3f));

            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_SPHERE)->draw(prog);
            M->popMatrix();
        }

        M->popMatrix();
    }
}

void Pawn::update(bool isKeyframe) {
    if (isKeyframe) {
        if (isDelayed) {
            delayCounter++;

            if (delayCounter > FRAME_RATE) {
                isDelayed = false;
                delayCounter = 0;
            }
        } else {
            pos = -(PAWN_JUMP_HEIGHT / 900.f) * (float) pow(frame - FRAME_RATE / 2, 2) + PAWN_JUMP_HEIGHT;
            frame++;

            if (frame > FRAME_RATE) {
                frame = 0;
                isDelayed = true;
            }
        }
    }
}


#define ROOK_OFFSET 0.2f
Rook::Rook() {
    pieceClass = Rank::ROOK;
};

void Rook::draw(shared_ptr<MatrixStack> M, shared_ptr<Program> prog) {
    {
        M->pushMatrix();
        M->translate(vec3(curXDelta, curYDelta, curZDelta));
        M->translate(vec3(2 * col, 1, -2 * row));

        {
            M->pushMatrix();
            M->translate(vec3(0, .05f, 0));
            M->scale(vec3(.6f, .05f, .6f));
            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_CYLINDER)->draw(prog);
            M->popMatrix();
        }

        {
            M->pushMatrix();
            M->rotate(rTheta, vec3(0, 1, 0));

            {
                M->pushMatrix();
                M->translate(vec3(0, 1.1f, 0));
                M->scale(vec3(.6f, .9f, .6f));

                glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
                shapes->at(SHAPE_CYLINDER)->draw(prog);
                M->popMatrix();
            }

            {
                M->pushMatrix();
                M->translate(vec3(0, 2, 0));

                float tx, tz, theta = 0;
                for (int i = 0; i < 6; i++) {
                    M->pushMatrix();

                    tx = .6f * sin(theta);
                    tz = .6f * cos(theta);

                    M->translate(vec3(tx, (i % 2 == 0) ? (0.1f - pos) : (0.1f - ROOK_OFFSET + pos), tz));
                    M->rotate(theta, vec3(0, 1, 0));
                    M->scale(vec3(.2f, .25f, .15f));
                    theta += 1.047f;

                    glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
                    shapes->at(SHAPE_CYLINDER)->draw(prog);
                    M->popMatrix();
                }

                M->popMatrix();
            }
            M->popMatrix();
        }

        M->popMatrix();
    }
}

void Rook::update(bool isKeyframe) {
    if (isKeyframe) {
        if (pos > ROOK_OFFSET || pos < 0) {
            direction = -direction;
        } 
        pos += direction * .005f;
        rTheta += .005f;
    }
}

Knight::Knight() {
    pieceClass = Rank::KNIGHT;
};

void Knight::draw(shared_ptr<MatrixStack> M, shared_ptr<Program> prog) {
    {
        M->pushMatrix();
        M->translate(vec3(curXDelta, curYDelta, curZDelta));
        M->translate(vec3(2 * col, 1, -2 * row));

        {
            M->pushMatrix();
            M->translate(vec3(0, .05f, 0));
            M->scale(vec3(.8f, .05f, .8f));
            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_CYLINDER)->draw(prog);
            M->popMatrix();
        }

        {
            M->pushMatrix();
            M->rotate(rTheta, vec3(0, 1, 0));

            {
                M->pushMatrix();
                M->translate(vec3(0, .9f, 0));
                M->scale(vec3(.8f, .8f, .8f));

                glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
                shapes->at(SHAPE_SPHERE)->draw(prog);
                M->popMatrix();
            }

            {
                M->pushMatrix();
                M->translate(vec3(0, 2.f, 0));
                M->rotate(radians(90.f), vec3(0, 0, 1));
                M->scale(vec3(.9f, .9f, .9f));

                glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
                shapes->at(SHAPE_SHELL)->draw(prog);
                M->popMatrix();
            }

            M->popMatrix();
        }

        M->popMatrix();
    }
}

void Knight::update(bool isKeyframe) {
    if (isKeyframe) {
        if (isKeyframe) {
            pos = .1f * sin(10 * rTheta);
            rTheta += .01f;
        }
    }
}

Bishop::Bishop() {
    pieceClass = Rank::BISHOP;
};

void Bishop::draw(shared_ptr<MatrixStack> M, shared_ptr<Program> prog) {
    {
        M->pushMatrix();
        M->translate(vec3(curXDelta, curYDelta, curZDelta));
        M->translate(vec3(2 * col, 1, -2 * row));

        {
            M->pushMatrix();
            M->translate(vec3(0, .05f, 0));
            M->scale(vec3(.8f, .05f, .8f));
            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_CYLINDER)->draw(prog);
            M->popMatrix();
        }

        {
            M->pushMatrix();
            M->translate(vec3(0, 1.4f, 0));
            M->scale(vec3(.8f, 1.2f, .8f));

            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_CONE)->draw(prog);
            M->popMatrix();
        }

        {
            M->pushMatrix();
            M->translate(vec3(0, 3.2f + pos, 0));
            M->rotate(rTheta, vec3(0, 1, 0));
            M->scale(vec3(.4f, .5f, .4f));

            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_DIAMOND)->draw(prog);
            M->popMatrix();
        }

        M->popMatrix();
    }
}

void Bishop::update(bool isKeyframe) {
    if (isKeyframe) {
        if (isKeyframe) {
            pos = .1f * sin(10*rTheta);
            rTheta += .01f;
        }
    }
}

Queen::Queen() {
    pieceClass = Rank::QUEEN;
};

void Queen::draw(shared_ptr<MatrixStack> M, shared_ptr<Program> prog) {
    {
        M->pushMatrix();
        M->translate(vec3(curXDelta, curYDelta, curZDelta));
        M->translate(vec3(2 * col, 1, -2 * row));

        {
            M->pushMatrix();
            M->translate(vec3(0, .05f, 0));
            M->scale(vec3(.7f, .05f, .7f));
            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_CYLINDER)->draw(prog);
            M->popMatrix();
        }

        {
            M->pushMatrix();
            M->translate(vec3(0, 1.2f, 0));
            M->rotate(-2 * rTheta, vec3(0, 1, 0));
            M->scale(vec3(1.8f, 2, 1.8f));

            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_SPRING)->draw(prog);
            M->popMatrix();
        }

        {
            M->pushMatrix();
            M->translate(vec3(0, 3.15f, 0));
            M->scale(vec3(.7f, .05f, .7f));
            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_CYLINDER)->draw(prog);
            M->popMatrix();
        }

        {
            M->pushMatrix();
            M->translate(vec3(0, 3.45f, 0));
            M->rotate(rTheta, vec3(0, 1, 0));

            float tx, tz, theta = 0;
            for (int i = 0; i < 10; i++) {
                M->pushMatrix();

                tx = .8f * sin(theta);
                tz = .8f * cos(theta);

                M->translate(vec3(tx, 0, tz));
                M->rotate(theta, vec3(0, 1, 0));
                M->rotate(radians(30.f), vec3(1, 0, 0));
                M->scale(vec3(.2f, .35f, .05f));
                theta += 0.628f;

                glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
                shapes->at(SHAPE_CONE)->draw(prog);
                M->popMatrix();
            }

            {
                M->pushMatrix();
                M->translate(vec3(0, .05f, 0));
                M->scale(vec3(.4f, .4f, .4f));
                glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
                shapes->at(SHAPE_SPHERE)->draw(prog);
                M->popMatrix();
            }

            M->popMatrix();
        }

        M->popMatrix();
    }
}

void Queen::update(bool isKeyframe) {
    if (isKeyframe) {
        if (isKeyframe) {
            rTheta += .01f;
        }
    }
}

King::King() {
    pieceClass = Rank::KING;
};

void King::draw(shared_ptr<MatrixStack> M, shared_ptr<Program> prog) {
    {
        M->pushMatrix();
        M->translate(vec3(curXDelta, curYDelta, curZDelta));
        M->translate(vec3(2 * col, 1, -2 * row));

        {
            M->pushMatrix();
            M->translate(vec3(0, .05f, 0));
            M->scale(vec3(.7f, .05f, .7f));
            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_CYLINDER)->draw(prog);
            M->popMatrix();
        }

        {
            M->pushMatrix();
            M->translate(vec3(0, 1.2f, 0));
            M->rotate(2 * rTheta, vec3(0, 1, 0));
            M->scale(vec3(1.8f, 2, 1.8f));

            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_SPRING)->draw(prog);
            M->popMatrix();
        }

        {
            M->pushMatrix();
            M->translate(vec3(0, 3.15f, 0));
            M->scale(vec3(.7f, .05f, .7f));
            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_CYLINDER)->draw(prog);
            M->popMatrix();
        }

        {
            M->pushMatrix();
            M->translate(vec3(0, 3.45f, 0));
            M->rotate(-rTheta, vec3(0, 1, 0));

            float tx, tz, theta = 0;
            for (int i = 0; i < 10; i++) {
                M->pushMatrix();

                tx = .8f * sin(theta);
                tz = .8f * cos(theta);

                M->translate(vec3(tx, 0, tz));
                M->rotate(theta, vec3(0, 1, 0));
                M->rotate(radians(30.f), vec3(1, 0, 0));
                M->scale(vec3(.2f, .35f, .05f));
                theta += 0.628f;

                glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
                shapes->at(SHAPE_CONE)->draw(prog);
                M->popMatrix();
            }

            M->popMatrix();
        }


        {
            M->pushMatrix();
            M->translate(vec3(0, 3.8f, 0));
            M->rotate(radians(90.f), vec3(0, 1, 0));
            M->scale(vec3(.5f, .6f, .5f));
            glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, value_ptr(M->topMatrix()));
            shapes->at(SHAPE_CROSS)->draw(prog);
            M->popMatrix();
        }

        M->popMatrix();
    }
}

void King::update(bool isKeyframe) {
    if (isKeyframe) {
        if (isKeyframe) {
            rTheta += .01f;
        }
    }
}
