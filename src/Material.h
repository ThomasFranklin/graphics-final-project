#pragma once

#ifndef LAB471_MATERIAL_H_INCLUDED
#define LAB471_MATERIAL_H_INCLUDED

#include <glad/glad.h>

class Material {

public:
    GLfloat kd[3];
    GLfloat ks[3];
    GLfloat shininess;
    GLfloat ambient[3];

};

#endif // LAB471_MATERIAL_H_INCLUDED