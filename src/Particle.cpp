
#include <iostream>
#include "Particle.h"
#include "GLSL.h"
#include "MatrixStack.h"
#include "Program.h"
#include "Texture.h"


float randFloat(float l, float h) {
    float r = rand() / (float) RAND_MAX;
    return (1.0f - r) * l + r * h;
}

// all particles born at the origin
void Particle::rebirth(int runtime, float x, float z, int colorProfile) {
    this->x.x = x;
    this->x.y = 2;
    this->x.z = z;
    v.x = randFloat(-6, 6);
    v.y = randFloat(0, 15);
    v.z = randFloat(-6, 6);
    lifespan = runtime;

    color.r = colorProfile == 1 ? randFloat(0.81f, .9f) : randFloat(0.66f, .82f);
    color.g = colorProfile == 1 ? randFloat(0.71f, 0.8f): randFloat(0.66f, .82f);
    color.b = colorProfile == 1 ? randFloat(0, 0.23f) : randFloat(0.66f, .82f);
    color.a = 1.0f;
}

void Particle::update(int time) {

    // very simple update
    v.y -= .2f;
    x.x += .015f * v.x;
    x.y += .015f * v.y;
    x.z += .015f * v.z;

    color.a = (lifespan - time) / (float) lifespan;
}
