#include "GameStateMachine.h"
#include "Constants.h"

using namespace constants;

GameStateMachine::GameStateMachine() {}

GameStateMachine::~GameStateMachine() {}

void GameStateMachine::init(GameBoard* board, Camera* camera) {
    this->board = board;
    this->camera = camera;
    isPaused = true;
    state = State::PLAYER1;
    previousPlayer = State::PLAYER2;
}

void GameStateMachine::pauseGame() {
    isPaused = true;
}

void GameStateMachine::resumeGame() {
    isPaused = false;
}

void GameStateMachine::endTurn() {
    board->prepareToMoveSelectedPiece();
    isPaused = true;
    previousPlayer = state;
    state = State::DELAY;
    delayTimer = 0;
}


void GameStateMachine::transition() {
    if (state == State::DELAY) {
        if (delayTimer > FRAME_RATE / 3) {
            state = State::MOVE_PIECE;
            board->beginMovingSelectedPiece();
        } else {
            delayTimer++;
        }
    }else if (state == State::CAM_REPOSITION) {
        if (camera->updateReposition()) {
            switch (previousPlayer) {
                case State::PLAYER1:
                    state = State::PLAYER2;
                    break;
                case State::PLAYER2:
                default:
                    state = State::PLAYER1;
                    break;
            }

            isPaused = false;
        }
    } else if (state == State::MOVE_PIECE) {
        if (board->moveSelectedPiece()) {
            if (board->endMovingSelectedPiece()) {
                state = State::GAME_OVER;
            } else {
                state = State::CAM_REPOSITION;
                switch (previousPlayer) {
                    case State::PLAYER1:
                        camera->prepareForReposition(Camera::SILVER_THETA, Camera::SILVER_ALPHA);
                        break;
                    case State::PLAYER2:
                    default:
                        camera->prepareForReposition(Camera::GOLD_THETA, Camera::GOLD_ALPHA);
                        break;
                }
            }
        }
    }
}

void GameStateMachine::changeSelectedPiece(int direction) {
    if (!isPaused) {
        switch (state) {
            case State::PLAYER1:
                board->changeSelectedPiece(direction, Color::Gold);
                break;
            case State::PLAYER2:
                board->changeSelectedPiece(direction, Color::Silver);
                break;
            default:
                break;
        }
    }
}

void GameStateMachine::changeSelectedSquare(int direction) {
    if (!isPaused) {
        board->changeSelectedSquare(direction);
    }
}


void GameStateMachine::resetSelection() {
    if (!isPaused) {
        board->resetSelectedPiece(true);
        board->resetEligibleMoves();
    }
}

void GameStateMachine::movePiece() {
    if (!isPaused) {
        if (board->canMoveSelectedPiece()) {
            endTurn();
        }
    }
}