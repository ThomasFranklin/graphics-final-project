Final Project - 3D Chess
By Thomas Franklin

Included are all the source files needed, plus are resources needed, to build my final project.
All of the Visual Studio files have been included, plus a CMake file.

The game is 2-player. The controls are:
 - WASD to Rotate the camera
 - Q/E to zoom in and out
 - LEFT/RIGHT arrow keys to select a piece (selected pice will be raised)
 - UP/DOWN arrow keys to select a target square
 - ENTER to move the selected piece to the selected square
 - ESC to quit the game