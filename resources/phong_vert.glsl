#version  330 core
layout(location = 0) in vec4 vertPos;
layout(location = 1) in vec3 vertNor;
uniform mat4 P;
uniform mat4 V;
uniform mat4 M;
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 ambient;
out vec3 wNor;
out vec3 lightVec;
out vec3 halfVec;

void main()
{
    gl_Position = P * V * M * vertPos;
	vec3 wPos = (V * M * vertPos).xyz;
	vec3 wLight = (V * vec4(lightPos, 0.0)).xyz;
	wNor = normalize((V * M * vec4(vertNor, 0.0)).xyz);
	lightVec = normalize(wLight - wPos);
	halfVec = normalize(lightVec - wPos);
}