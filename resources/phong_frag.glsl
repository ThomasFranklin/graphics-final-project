#version 330 core 
in vec3 wNor;
in vec3 lightVec;
in vec3 halfVec;
uniform vec3 kd;
uniform vec3 lightColor;
uniform vec3 ks;
uniform float shininess;
uniform vec3 ambient;
out vec4 color;

void main()
{
	vec3 wNorNormed = normalize(wNor);
	vec3 lightVecNormed = normalize(lightVec);
	vec3 halfNormed = normalize(halfVec);

	vec3 diffuse = kd * max(0, dot(wNorNormed, lightVecNormed)) * lightColor;
	vec3 specular = ks * pow(max(0, dot(halfNormed, wNorNormed)), shininess) * lightColor;
	color = vec4(diffuse + specular + ambient, 1.0);
}
